# LVL UP SDK Java

## Installation

### Requirements

- Java 1.8+

### Maven users

Add this dependency and repository to your project

```xml
<dependencies>
    <dependency>
        <groupId>pro.lvlup.sdk</groupId>
        <artifactId>lvlup-sdk-java</artifactId>
        <version>0.0.2</version>
    </dependency>
</dependencies>


<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/17892980/packages/maven</url>
    </repository>
</repositories>
```

## Usage

```java
import pro.lvlup.sdk.UpClient;
import pro.lvlup.sdk.exception.ErrorException;
import pro.lvlup.sdk.model.request.NewPaymentRequest;
import pro.lvlup.sdk.model.response.NewPaymentResponse;
import pro.lvlup.sdk.model.response.PaymentInfoResponse;

public class UpExample {

    public static void main(String[] args) {
        UpClient upClient = new UpClient("api_key_here");

        //Enable sandbox mode
        upClient.setSandboxMode(true);

        try {
            //Create new payment
            NewPaymentRequest newPaymentRequest = new NewPaymentRequest();
            newPaymentRequest.setAmount("13.37");
            newPaymentRequest.setRedirectUrl("https://example.com/site/donation-site");
            newPaymentRequest.setWebhookUrl("https://example.com/site/notify");

            NewPaymentResponse newPayment = upClient.createLinkForPayment(newPaymentRequest);
            System.out.println(newPayment.getUrl());

            //Get payment info
            PaymentInfoResponse paymentInfo = upClient.getPaymentInfo(newPayment.getId());
            System.out.println(paymentInfo.isPayed());
        } catch(ErrorException e) {
            System.out.println(e.getCode() + ": " + e.getMsg());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```
