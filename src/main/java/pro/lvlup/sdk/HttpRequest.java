package pro.lvlup.sdk;

import com.google.gson.Gson;
import okhttp3.*;
import pro.lvlup.sdk.exception.ErrorException;
import pro.lvlup.sdk.model.Error;

import java.io.IOException;
import java.util.Map;

public class HttpRequest {

    private OkHttpClient client;
    private UpProperties upProperties;
    private UpClient upClient;
    private String apiKey;

    private Gson gson = new Gson();
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public HttpRequest(UpClient client, String apiKey) {
        this.client = new OkHttpClient();
        this.upProperties = new UpProperties();
        this.apiKey = apiKey;
        this.upClient = client;
    }

    public String get(String requestUri) throws ErrorException, IOException {
        Request request = new Request.Builder()
                .url(this.upClient.getBaseUrl() + requestUri)
                .addHeader("Authorization", "Bearer " + this.apiKey)
                .addHeader("User-Agent", String.format("%s %s", this.upProperties.getArtifactId(),
                        this.upProperties.getVersion()))
                .build();

        return call(request);
    }

    public String get(String requestUri, Map<String, String> queryParams) throws ErrorException, IOException {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(this.upClient.getBaseUrl() + requestUri).newBuilder();
        queryParams.forEach(urlBuilder::addQueryParameter);

        Request request = new Request.Builder()
                .url(urlBuilder.build())
                .addHeader("Authorization", "Bearer " + this.apiKey)
                .addHeader("User-Agent", String.format("%s %s", this.upProperties.getArtifactId(),
                        this.upProperties.getVersion()))
                .build();
        return call(request);
    }

    public String post(String requestUri, Object obj) throws ErrorException, IOException {
        String json = this.gson.toJson(obj);
        RequestBody body = RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url(this.upClient.getBaseUrl() + requestUri)
                .addHeader("Authorization", "Bearer " + this.apiKey)
                .addHeader("User-Agent", String.format("%s %s", this.upProperties.getArtifactId(),
                        this.upProperties.getVersion()))
                .post(body)
                .build();
        return call(request);
    }

    private String call(Request request) throws ErrorException, IOException {
        Response response = this.client.newCall(request).execute();
        if (!response.isSuccessful()) {
            if (response.body() != null) {
                Error error = gson.fromJson(response.body().string(), Error.class);
                throw new ErrorException(error.getCode(), error.getMsg());
            }

            throw new ErrorException(-1, "Invalid error occured");
        }

        //TODO: string() may procude null pointer exception
        return response.body().string();
    }
}
