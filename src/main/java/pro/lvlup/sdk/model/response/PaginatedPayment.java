package pro.lvlup.sdk.model.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class PaginatedPayment {

    private int id;
    private int methodId;
    private int serviceId;
    private String amount;;
    private String description;
    private Date createdAt;
}
