package pro.lvlup.sdk.model.request;

import lombok.*;
import pro.lvlup.sdk.WebhookRequestStatus;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class WebhookRequest {
    private String paymentId;
    private String status;
}
