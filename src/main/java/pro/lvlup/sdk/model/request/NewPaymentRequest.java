package pro.lvlup.sdk.model.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class NewPaymentRequest {
    private String amount;
    private String redirectUrl;
    private String webhookUrl;
}
