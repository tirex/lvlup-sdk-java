package pro.lvlup.sdk;

import com.google.gson.Gson;
import pro.lvlup.sdk.exception.ErrorException;
import pro.lvlup.sdk.model.request.NewPaymentRequest;
import pro.lvlup.sdk.model.response.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UpClient implements UpApi {

    private static final String API_BASE_URL = "https://api.lvlup.pro/v4";
    private static final String SANDBOX_BASE_URL = "https://sandbox-api.lvlup.pro/v4";

    private String baseUrl = API_BASE_URL;

    private HttpRequest httpRequest;
    private Gson gson = new Gson();

    private boolean sandboxMode = false;

    public UpClient(String apiKey) {
        this.httpRequest = new HttpRequest(this, apiKey);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public boolean isSandboxMode() {
        return sandboxMode;
    }

    public void setSandboxMode(boolean sandboxMode) {
        if(sandboxMode) {
            this.baseUrl = SANDBOX_BASE_URL;
        } else {
            this.baseUrl = API_BASE_URL;
        }

        this.sandboxMode = sandboxMode;
    }

    @Override
    public ProfileResponse getProfileInfo() throws ErrorException, IOException {
        String response = this.httpRequest.get("/me");
        return gson.fromJson(response, ProfileResponse.class);
    }

    @Override
    public WalletBalanceResponse getWalletBalance() throws ErrorException, IOException {
        String response = this.httpRequest.get("/wallet");
        return gson.fromJson(response, WalletBalanceResponse.class);
    }

    @Override
    public PaymentsListResponse getPaymentsList(int limit, int afterId, int beforeId) throws ErrorException, IOException {
        Map<String, String> map = new HashMap<String, String>();
        map.put("limit", String.valueOf(limit));
        map.put("afterId", String.valueOf(afterId));
        map.put("beforeId", String.valueOf(beforeId));

        String response = this.httpRequest.get("/payments", map);
        return gson.fromJson(response, PaymentsListResponse.class);
    }

    @Override
    public NewPaymentResponse createLinkForPayment(NewPaymentRequest newPaymentRequest) throws ErrorException, IOException {
        String response = this.httpRequest.post("/wallet/up", newPaymentRequest);
        return gson.fromJson(response, NewPaymentResponse.class);
    }

    @Override
    public PaymentInfoResponse getPaymentInfo(String id) throws ErrorException, IOException {
        String response = this.httpRequest.get("/wallet/up/" + id);
        return gson.fromJson(response, PaymentInfoResponse.class);
    }
}
