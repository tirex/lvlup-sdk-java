package pro.lvlup.sdk;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import pro.lvlup.sdk.exception.ErrorException;
import pro.lvlup.sdk.model.request.WebhookRequest;

public class UpWebhookParser {

    private static final Gson GSON = new Gson();

    private UpWebhookParser() {
    }

    public static WebhookRequest parse(String json) throws ErrorException {
        try {
            return GSON.fromJson(json, WebhookRequest.class);
        } catch (JsonParseException e) {
            throw new ErrorException(-1, "Error occured while parsing json");
        }
    }
}